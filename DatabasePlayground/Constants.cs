﻿namespace DatabasePlayground
{
    public class Constants
    {
        public const string MssqlConnectionKey = "MssqlConnection";
        public const string PostgresqlConnectionKey = "PostgresqlConnection";
    }
}