﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper;
using DatabasePlayground.Database.Models;

namespace DatabasePlayground.Database.Repositories
{
    sealed class DapperMssqlCustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly IDbConnection _db = new SqlConnection(ConfigurationManager.ConnectionStrings[Constants.MssqlConnectionKey].ConnectionString);
        private bool _disposed;

        public async Task<IEnumerable<Customer>> GetCustomers(int amount, string sort)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");

            IEnumerable<Customer> results = await _db.QueryAsync<Customer>("SELECT TOP " + amount + " [CustomerID]," +
                                                                          "[CustomerFirstName],[CustomerLastName],[IsActive] " +
                                                                          "FROM [Customer] ORDER BY CustomerID " + sort);
            return results;
        }

        public async Task<Customer> GetSingleCustomer(int customerId)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");
            if (customerId <= 0) throw new ArgumentException();

            Customer customer =
                await _db.QueryFirstAsync<Customer>("SELECT [CustomerID],[CustomerFirstName],[CustomerLastName],[IsActive] " +
                                                   "FROM [Customer] WHERE CustomerID = @CustomerID",
                    new {CustomerID = customerId});

            return customer;
        }

        public async Task<int> InsertCustomer(Customer ourCustomer)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");

            var affectedRows = await _db.QuerySingleAsync<int>(@"INSERT Customer([CustomerFirstName],[CustomerLastName],[IsActive]) 
                                                       values (@CustomerFirstName, @CustomerLastName, @IsActive);
                SELECT CAST(SCOPE_IDENTITY() as int)", 
                new { ourCustomer.CustomerFirstName, ourCustomer.CustomerLastName, IsActive = true });

            return affectedRows;
        }

        public async Task<bool> DeleteCustomer(int customerId)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");
            if (customerId <= 0) throw new ArgumentException();

            int rowsAffected = await _db.ExecuteAsync(@"DELETE FROM [Customer] WHERE CustomerID = @CustomerID", new { CustomerID = customerId });
            return rowsAffected > 0;
        }

        public async Task<bool> UpdateCustomer(Customer ourCustomer)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");
            if (ourCustomer.CustomerID <= 0) throw new ArgumentException();

            int rowsAffected = await _db.ExecuteAsync("UPDATE [Customer] " +
                                                     "SET [CustomerFirstName] = @CustomerFirstName " +
                                                     ",[CustomerLastName] = @CustomerLastName" +
                                                     ", [IsActive] = @IsActive " +
                                                     "WHERE CustomerID = " + ourCustomer.CustomerID, ourCustomer);

            return rowsAffected > 0;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (_disposed)
                return;

            if (isDisposing)
            {
                _db.Dispose();
                _disposed = true;
            }
        }
    }
}