﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using Dapper;
using DatabasePlayground.Database.Models;
using Npgsql;

namespace DatabasePlayground.Database.Repositories
{
    public class DapperPostgresqlCustomerRepository : ICustomerRepository, IDisposable
    {
        private readonly IDbConnection _db = new NpgsqlConnection(ConfigurationManager.ConnectionStrings[Constants.PostgresqlConnectionKey].ConnectionString);
        private bool _disposed;

        public async Task<IEnumerable<Customer>> GetCustomers(int amount, string sort)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");

            IEnumerable<Customer> results = await _db.QueryAsync<Customer>("SELECT \"CustomerID\"," +
                                                                           "\"CustomerFirstName\", \"CustomerLastName\", \"IsActive\" " +
                                                                           "FROM public.\"Customer\" ORDER BY \"CustomerID\" " + sort + " LIMIT " + amount);
            return results;
        }

        public async Task<Customer> GetSingleCustomer(int customerId)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");
            if (customerId <= 0) throw new ArgumentException();

            Customer customer =
                await _db.QueryFirstAsync<Customer>("SELECT \"CustomerID\", \"CustomerFirstName\", \"CustomerLastName\", \"IsActive\" " +
                                                    "FROM public.\"Customer\" WHERE \"CustomerID\" = @CustomerID",
                    new { CustomerID = customerId });

            return customer;
        }

        public async Task<int> InsertCustomer(Customer ourCustomer)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");

            var affectedRows = await _db.QuerySingleAsync<int>("INSERT INTO public.\"Customer\" (\"CustomerFirstName\", \"CustomerLastName\", \"IsActive\") " +
                                                               "values (@CustomerFirstName, @CustomerLastName, @IsActive) RETURNING \"CustomerID\"",
                new { ourCustomer.CustomerFirstName, ourCustomer.CustomerLastName, IsActive = true });

            return affectedRows;
        }

        public async Task<bool> DeleteCustomer(int customerId)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");
            if (customerId <= 0) throw new ArgumentException();

            int rowsAffected = await _db.ExecuteAsync("DELETE FROM public.\"Customer\" WHERE \"CustomerID\" = @CustomerID", new { CustomerID = customerId });
            return rowsAffected > 0;
        }

        public async Task<bool> UpdateCustomer(Customer ourCustomer)
        {
            if (_disposed) throw new ObjectDisposedException("Connection has been disposed");
            if (ourCustomer.CustomerID <= 0) throw new ArgumentException();

            int rowsAffected = await _db.ExecuteAsync("UPDATE public.\"Customer\" " +
                                                      "SET \"CustomerFirstName\" = @CustomerFirstName " +
                                                      ",\"CustomerLastName\" = @CustomerLastName" +
                                                      ", \"IsActive\" = @IsActive " +
                                                      "WHERE \"CustomerID\" = " + ourCustomer.CustomerID, ourCustomer);

            return rowsAffected > 0;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool isDisposing)
        {
            if (_disposed)
                return;

            if (isDisposing)
            {
                _db.Dispose();
                _disposed = true;
            }
        }

    }
}