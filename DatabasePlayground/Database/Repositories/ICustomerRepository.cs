﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DatabasePlayground.Database.Models;

namespace DatabasePlayground.Database.Repositories
{
    interface ICustomerRepository
    {
        Task<IEnumerable<Customer>> GetCustomers(int amount, string sort);

        Task<Customer> GetSingleCustomer(int customerId);

        Task<int> InsertCustomer(Customer ourCustomer);

        Task<bool> DeleteCustomer(int customerId);

        Task<bool> UpdateCustomer(Customer ourCustomer);
    }
}
