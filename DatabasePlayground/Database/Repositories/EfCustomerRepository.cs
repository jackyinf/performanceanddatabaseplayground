﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DatabasePlayground.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace DatabasePlayground.Database.Repositories
{
    public class EfCustomerRepository //: ICustomerRepository
    {
        public Task<List<Customer>> GetCustomers(int amount, string sort) => new EfCoreContext().Customers.Take(amount).ToListAsync();

        public Task<Customer> GetSingleCustomer(int customerId) => new EfCoreContext().Customers.SingleAsync(x => x.CustomerID == customerId);

        public Task<int> InsertCustomer(Customer ourCustomer) => new EfCoreContext().Customers.AddAsync(ourCustomer).ContinueWith(t => ourCustomer.CustomerID);

        public Task<bool> DeleteCustomer(int customerId)
        {
            var context = new EfCoreContext();
            context.Customers.Remove(context.Find<Customer>(customerId));
            return Task.FromResult(true);
        }

        public Task<bool> UpdateCustomer(Customer ourCustomer) => Task.Run(() => new EfCoreContext().Customers.Update(ourCustomer) != null);
    }
}