﻿using System.Configuration;
using DatabasePlayground.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace DatabasePlayground.Database
{
    public class EfCoreContext : DbContext
    {
        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(ConfigurationManager.ConnectionStrings[Constants.PostgresqlConnectionKey].ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(entity =>
            {
                entity.ToTable("Customer");
            });
        }
    }
}