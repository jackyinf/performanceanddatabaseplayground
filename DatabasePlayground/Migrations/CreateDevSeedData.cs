﻿using FluentMigrator;

namespace DatabasePlayground.Migrations
{
    [Profile("Development")]
    public class CreateDevSeedData : Migration
    {
        public override void Up()
        {
            Insert.IntoTable("User").Row(new
            {
                Username = "devuser1",
                DisplayName = "Dev User"
            });
        }

        public override void Down()
        {
            //empty, not using
        }
    }
}