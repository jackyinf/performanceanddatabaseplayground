﻿using FluentMigrator;

namespace DatabasePlayground.Migrations
{
    [Migration(2)]
    public class Migration2 : Migration
    {
        public override void Up()
        {
            Create.Table("Customer")
                .WithColumn("CustomerID").AsInt32().NotNullable().PrimaryKey().Identity()
                .WithColumn("CustomerFirstName").AsString(255).Nullable()
                .WithColumn("CustomerLastName").AsString(255).Nullable()
                .WithColumn("IsActive").AsBoolean().NotNullable().WithDefaultValue(false);

            Create.Index("ix_CustomerFirstName").OnTable("Customer").OnColumn("CustomerFirstName").Ascending()
                .WithOptions().NonClustered();
        }

        public override void Down()
        {
            Delete.Table("Customer");
        }
    }
}