﻿using System.Configuration;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;

namespace DatabasePlayground.Migrations.Execution
{
    public class MigrationExecutor
    {
        private const string SolutionPathPrefix = @"C:\Users\zekar\Documents\Visual Studio 2017\Projects\Performance\";
        private const string MigratorExePath = @"packages\FluentMigrator.Tools.1.6.2\tools\AnyCPU\40\Migrate.exe";

        private const string MssqlMigrationKey = "Migration_MssqlConnection";
        private const string PostgresqlMigrationKey = "Migration_PostgresqlConnection";

        /*
         * Postgres:
         * 
            Migrate.exe 
                --conn="User ID=postgres;Password=root;Host=localhost;Port=5432;Database=DatabasePlaygroundDemo1;Pooling=true;" 
                --provider="postgres" 
                --assembly="../../../../../DatabasePlayground/bin/Debug/DatabasePlayground.dll" 
                --task="migrate"

            * 
            * Mssql:
            *
            Migrate.exe 
                --conn="server=.\\SQLEXPRESS;uid=qwe;pwd=qwe;database=DatabasePlaygroundDemo1" --provider="sqlserver2014" --assembly="../../../../../DatabasePlayground/bin/Debug/DatabasePlayground.dll" --task="migrate"
        */

        [Test]
        public void ExecuteMigration()
        {
            var connectionStringSettings = ConfigurationManager.ConnectionStrings[MssqlMigrationKey];

            var process = new Process();
            process.StartInfo.FileName = $"{SolutionPathPrefix}{MigratorExePath}";
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.Arguments =
                $"--conn=\"{connectionStringSettings.ConnectionString}\" " +
                $"--provider=\"{connectionStringSettings.ProviderName}\" " +
                "--assembly=\"DatabasePlayground/bin/Debug/DatabasePlayground.dll\" " +
                "--task=\"migrate\"";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.WorkingDirectory = SolutionPathPrefix;
            process.OutputDataReceived += (sender, e) => Trace.WriteLine(e.Data);
            process.ErrorDataReceived += (sender, e) => Trace.WriteLine(e.Data);
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            int result = process.ExitCode;
            Assert.AreEqual(0, result);
        }
    }
}