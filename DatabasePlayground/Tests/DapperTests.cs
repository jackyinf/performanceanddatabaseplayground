﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using Dapper;
using DatabasePlayground.Database.Models;
using DatabasePlayground.Database.Repositories;
using NUnit.Framework;

namespace DatabasePlayground.Tests
{
    public class DapperDemo
    {
        [Test]
        public static void SimpleMssqlDapperTest()
        {
            var connectionString = ConfigurationManager.ConnectionStrings[Constants.MssqlConnectionKey].ConnectionString;
            IDbConnection db = new SqlConnection(connectionString);
            string query = "SELECT TOP 100 [CustomerID],[CustomerFirstName],[CustomerLastName],[IsActive] FROM [Customer]";
            IEnumerable<Customer> ourCustomers = db.Query<Customer>(query);

            foreach (var customer in ourCustomers)
            {
                Trace.WriteLine(new string('*', 20));
                Trace.WriteLine("\nCustomer ID: " + customer.CustomerID.ToString());
                Trace.WriteLine("First Name: " + customer.CustomerFirstName);
                Trace.WriteLine("Last Name: " + customer.CustomerLastName);
                Trace.WriteLine("Is Active? " + customer.IsActive + "\n");
                Trace.WriteLine(new string('*', 20));
            }
        }

        [Test]
        public void DapperMssqlRepositoryTest()
        {
            ICustomerRepository repo = new DapperMssqlCustomerRepository();
            var result1 = repo.InsertCustomer(new Customer { CustomerFirstName = "Blabla", CustomerLastName = "Hehe", IsActive = true}).Result;
            var result2 = repo.GetCustomers(10, "asc").Result;
            var result3 = repo.GetSingleCustomer(result1).Result;
            var result4 = repo.UpdateCustomer(new Customer { CustomerID = result1, CustomerFirstName = "Blabla11", CustomerLastName = "Hehe11", IsActive = true }).Result;
            var result5 = repo.DeleteCustomer(result1).Result;
        }

        [Test]
        public void DapperPostgresqllRepositoryTest()
        {
            ICustomerRepository repo = new DapperPostgresqlCustomerRepository();
            var result1 = repo.InsertCustomer(new Customer { CustomerFirstName = "Blabla", CustomerLastName = "Hehe", IsActive = true}).Result;
            var result2 = repo.GetCustomers(10, "asc").Result;
            var result3 = repo.GetSingleCustomer(result1).Result;
            var result4 = repo.UpdateCustomer(new Customer { CustomerID = result1, CustomerFirstName = "Blabla11", CustomerLastName = "Hehe11", IsActive = true }).Result;
            var result5 = repo.DeleteCustomer(result1).Result;
        }
    }
}