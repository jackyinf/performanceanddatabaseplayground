﻿using System.Linq;
using DatabasePlayground.Database;
using DatabasePlayground.Database.Models;
using DatabasePlayground.Database.Repositories;
using NUnit.Framework;

namespace DatabasePlayground.Tests
{
    public class EfTests
    {
        [Test]
        public void EfCanQuery()
        {
            var context = new EfCoreContext();
            var allCustomers = context.Customers.ToList();
        }

        [Test]
        public void EfCanAdd()
        {
            var context = new EfCoreContext();
            context.Customers.Add(new Customer { CustomerFirstName = "ten", CustomerLastName = "Ben", IsActive = false});
            context.SaveChanges();
        }

        [Test]
        public void EfCanCrud()
        {
            EfCustomerRepository repo = new EfCustomerRepository();
            var result1 = repo.InsertCustomer(new Customer { CustomerFirstName = "Blabla", CustomerLastName = "Hehe", IsActive = true }).Result;
            var result2 = repo.GetCustomers(10, "asc").Result;
            var id = result2.Last().CustomerID;
            var result3 = repo.GetSingleCustomer(id).Result;
            var result4 = repo.UpdateCustomer(new Customer { CustomerID = id, CustomerFirstName = "Blabla11", CustomerLastName = "Hehe11", IsActive = true }).Result;
            var result5 = repo.DeleteCustomer(id).Result;
        }
    }
}