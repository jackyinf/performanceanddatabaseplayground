﻿using System;
using System.IO;

namespace Performance.Other
{
    public class MemoryDemo
    {
        public static void Run()
        {
            var memoryStream = new MemoryStream();
            var segment = new ArraySegment<byte>(memoryStream.GetBuffer(), 100, 1024);
            // ...
            var blockStream = new MemoryStream(segment.Array, segment.Offset, segment.Count);
        }
    }
}