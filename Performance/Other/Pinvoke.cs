﻿using System;
using System.Runtime.InteropServices;

namespace Performance.Other
{
    public class Pinvoke
    {
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int puts(string c);

        public static void Run()
        {
            puts("Test");
        }

        [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr CreateFontIndirect(
            [In, MarshalAs(UnmanagedType.LPStruct)]
            LOGFONT lplf   // characteristics
        );

        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(
            IntPtr handle
        );

        public static void Run2()
        {
            LOGFONT lf = new LOGFONT();
            lf.lfHeight = 9;
            lf.lfFaceName = "Arial";
            IntPtr handle = CreateFontIndirect(lf);

            if (IntPtr.Zero == handle)
            {
                Console.WriteLine("Can't creates a logical font.");
            }
            else
            {

                if (IntPtr.Size == 4)
                    Console.WriteLine("{0:X}", handle.ToInt32());
                else
                    Console.WriteLine("{0:X}", handle.ToInt64());

                // Delete the logical font created.
                if (!DeleteObject(handle))
                    Console.WriteLine("Can't delete the logical font");
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    public class LOGFONT
    {
        public const int LF_FACESIZE = 32;
        public int lfHeight;
        public int lfWidth;
        public int lfEscapement;
        public int lfOrientation;
        public int lfWeight;
        public byte lfItalic;
        public byte lfUnderline;
        public byte lfStrikeOut;
        public byte lfCharSet;
        public byte lfOutPrecision;
        public byte lfClipPrecision;
        public byte lfQuality;
        public byte lfPitchAndFamily;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = LF_FACESIZE)]
        public string lfFaceName;
    }
}