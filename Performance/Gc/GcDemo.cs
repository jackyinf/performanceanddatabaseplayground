﻿using System;

namespace Performance.Gc
{
    class Point
    {
        public int x;
        public int y;
    }

    public class GcDemo
    {
        public void Run()
        {
            GC.Collect(2, GCCollectionMode.Forced);
            
        }
    }
}