﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;

namespace Performance.Gc
{
    public class LongRunningProgrammDemo
    {
        public static readonly List<string> Times = new List<string>();
        public static void Run()
        {
            Console.WriteLine("Press any key to exit");
            while (!Console.KeyAvailable)
            {
                Times.Add(DateTime.Now.ToString(CultureInfo.InvariantCulture));
                Console.Write('.');
                Thread.Sleep(1000);
            }
        }
    }
}