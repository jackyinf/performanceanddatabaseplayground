﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace Performance.Async
{
    public class AsyncCompressDemo
    {
        public static async Task AsyncCompress(IEnumerable<string> fileList)
        {
            var buffer = new byte[16384];
            foreach (var file in fileList)
                using (var inputStream = File.OpenRead(file))
                using (var outputStream = File.OpenWrite(file + ".compressed"))
                using (var compressStream = new GZipStream(outputStream, CompressionMode.Compress))
                {
                    var read = 0;
                    while ((read = await inputStream.ReadAsync(buffer, 0, buffer.Length)) > 0)
                        await compressStream.WriteAsync(buffer, 0, read);
                }
        }
    }
}