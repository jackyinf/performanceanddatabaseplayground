﻿using System;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using System.Threading.Tasks;

namespace Performance.Async
{
    public class TasksDemo
    {
        public delegate string AsyncMethodCaller();

        public void Run()
        {
            var tsc = new TaskCompletionSource<string>();
            var t = Task.Factory.StartNew(() => InnerRun(tsc));
            Console.WriteLine(tsc.Task.Result);
        }

        public async Task InnerRun(TaskCompletionSource<string> tsc)
        {
            AsyncMethodCaller caller = () => "test123";
            var task1 = Task<string>.Factory.FromAsync((callback, o) => caller.BeginInvoke(callback, null), result => "test234", TaskCreationOptions.None);
            var task2 = Task<int>.Factory.FromAsync(task1, result => 1 + ((Task<string>)result).Result.Length);
            await task2.ContinueWith(task3 =>
            {
                Thread.Sleep(300);
                Console.WriteLine(task3.Result);
            });
            Console.WriteLine("lol");
            tsc.TrySetResult("blabla");
        }
    }
}