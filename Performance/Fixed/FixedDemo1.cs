﻿using System;
using Performance.Gc;

namespace Performance.Fixed
{
    public class FixedDemo1
    {
        // Unsafe method: takes a pointer to an int.
        static unsafe void SquarePtrParam(int* p)
        {
            *p *= *p;
        }

        public static unsafe void Run()
        {
            Point pt = new Point();
            pt.x = 5;
            pt.y = 6;
            // Pin pt in place:
            fixed (int* p = &pt.x)
            {
                SquarePtrParam(p);
            }
            // pt now unpinned.
            Console.WriteLine("{0} {1}", pt.x, pt.y);
        }

        public unsafe void FixedMethod()
        {
            var pt = new Point();
            fixed (int* p = &pt.x)
            {
                *p = 1;
            }
        }
    }
}